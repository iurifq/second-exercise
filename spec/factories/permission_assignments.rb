FactoryGirl.define do
  factory :permission_assignment do
    permission { FactoryGirl.build(:permission) }

    factory :user_permission_assignment do
      assignee { FactoryGirl.create(:user) }
    end

    factory :role_permission_assignment do
      assignee { FactoryGirl.create(:role) }
    end
  end
end
