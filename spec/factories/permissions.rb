FactoryGirl.define do
  factory :permission do
    name { Faker::Lorem.word }
  end
end

