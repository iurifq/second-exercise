require 'spec_helper'

describe User do
  it { should belong_to :role }
  it { should have_many :permission_assignments }
  it { should validate_presence_of :name }
  it { should_not allow_mass_assignment_of :name }

  describe "#permissions" do
    let(:user) { FactoryGirl.create(:user) }
    let(:role_permission_assignment) { FactoryGirl.create(:role_permission_assignment, assignee: user.role)}
    let(:other_role_permission_assignment) { FactoryGirl.create(:role_permission_assignment)}
    let(:user_permission_assignment) { FactoryGirl.create(:user_permission_assignment, assignee: user)}
    let(:other_user_permission_assignment) { FactoryGirl.create(:user_permission_assignment)}
    let!(:role_permission) { role_permission_assignment.permission }
    let!(:user_permission) { user_permission_assignment.permission }
    let!(:other_users_permission) { other_user_permission_assignment.permission }
    let!(:other_roles_permission) { other_role_permission_assignment.permission }

    subject { user.permissions }

    it "includes user permission" do
      expect(subject).to include(user_permission)
    end

    it "includes role permission" do
      expect(subject).to include(role_permission)
    end

    it "does not include other users permissions" do
      expect(subject).not_to include(other_users_permission)
    end

    it "does not include other roles permissions" do
      expect(subject).not_to include(other_roles_permission)
    end
  end

  describe ".all_names" do
    let!(:user_names) { FactoryGirl.create_list(:user, 5).map(&:name) }

    it "includes all user names" do
      expect( User.all_names ).to include(*user_names)
    end

    it "does not include names that are not from users" do
      expect(user_names).to include(* User.all_names)
    end
  end
end
