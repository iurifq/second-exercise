require 'spec_helper'

describe Permission do
  it { should have_many :permission_assignments }

  describe ".name_begins_with" do
    let!(:permission_with_letter_a) { FactoryGirl.create(:permission, name: "a permission that begins with 'a'")}
    let!(:permission_without_letter_a) { FactoryGirl.create(:permission, name: "permission that does not begin with 'a'")}

    subject { Permission.name_begins_with('abc') }

    it "includes permissions with name beginning with given letter" do
      expect(subject).to include(permission_with_letter_a)
    end

    it "does not include permissions with name beginning with given letter" do
      expect(subject).not_to include(permission_without_letter_a)
    end

    it "returns nothing when empty string is given" do
      expect(Permission.name_begins_with("")).to be_empty
    end
  end
end
