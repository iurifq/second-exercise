require 'spec_helper'

describe PermissionAssignment do
  it { should belong_to :assignee }
  it { should belong_to :permission }
end
