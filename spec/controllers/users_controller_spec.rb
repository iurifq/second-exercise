require 'spec_helper'

describe UsersController do

  describe "GET 'index'" do

    let(:user_names) { ["name 1", "name 2"] }

    before do
      User.stub(all_names: user_names )
    end

    it "returns http success" do
      get 'index'
      response.should be_success
    end

    it "assigns @user_names with all user names" do
      get 'index'
      expect(assigns(:user_names)).to include(*user_names)
    end

    it "assigns @new_user with the new user" do
      get 'index'
      expect(assigns(:new_user)).not_to be_persisted
    end
  end

  describe "POST 'create'" do
    it "creates the user" do
      post 'create', user: { name: "some name", status: "active", role_id: 1}
    end
  end
end
