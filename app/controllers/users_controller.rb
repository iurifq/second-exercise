class UsersController < ApplicationController
  def index
    @user_names = User.all_names
    @new_user = User.new
  end

  def create
    user = User.new(params[:user].except(:name))
    user.name = params[:user][:name]
    if user.save
      @user_name = user.name
    else
      @errors = user.errors
    end
  end
end
