class User < ActiveRecord::Base
  attr_accessible :status, :role_id

  belongs_to :role
  has_many :permission_assignments, as: :assignee

  validates_presence_of :name

  def permissions
    PermissionAssignment.where("(assignee_id = ? AND assignee_type = ?) OR (assignee_id = ? AND assignee_type = ?)", id, User.to_s, role.id, Role.to_s).
      includes(:permission).
      map(&:permission)
  end

  def self.all_names
    User.all.map(&:name)
  end
end
