class PermissionAssignment < ActiveRecord::Base
  belongs_to :permission
  belongs_to :assignee, polymorphic: true
end
