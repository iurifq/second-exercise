class Permission < ActiveRecord::Base
  attr_accessible :name

  has_many :permission_assignments

  scope :name_begins_with, ->(string) do
    if string.empty?
      where("1 = 0")
    else
      where("name LIKE '#{string.first}%'")
    end
  end
end
