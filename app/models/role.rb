class Role < ActiveRecord::Base
  attr_accessible :name

  has_many :users
  has_many :permission_assignments, as: :assignee
end
