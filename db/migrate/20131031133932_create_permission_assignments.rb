class CreatePermissionAssignments < ActiveRecord::Migration
  def change
    create_table :permission_assignments do |t|
      t.string :assignee_type
      t.integer :assignee_value
      t.integer :permission_id
    end

    add_index :permission_assignments, :permission_id
    add_index :permission_assignments, [:assignee_type, :assignee_value], name: "index_permission_assignments_on_assignees"
  end
end
