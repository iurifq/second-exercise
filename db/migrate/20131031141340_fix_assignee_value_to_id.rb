class FixAssigneeValueToId < ActiveRecord::Migration
  def up
    remove_index :permission_assignments, name: "index_permission_assignments_on_assignees"
    remove_column :permission_assignments, :assignee_value
    add_column :permission_assignments, :assignee_id, :integer

    add_index "permission_assignments", ["assignee_type", "assignee_id"], :name => "index_permission_assignments_on_assignees"
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
